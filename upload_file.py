import dropbox
import os, sys

dbx = dropbox.Dropbox('')

if len(sys.argv) < 2:
    print "Usage: upload_file file_name"
    exit(1)

dropbox_destination = '/DB'

CHUNK_SIZE = 16 * 1024 * 1024

filename = os.path.basename(sys.argv[1])
dropbox_path = dropbox_destination + '/' + filename
file_size = os.path.getsize(sys.argv[1])

with open(sys.argv[1],'rb') as f:
    try:                                                                                                                                                                      
        print "Uploading sys.argv[1]"
        if file_size < CHUNK_SIZE:                                                                                                                                            
            dbx.files_upload(f, dropbox_path)                                                                                                                                 
        else:                                                                                                                                                                 
            sess = dbx.files_upload_session_start(f.read(CHUNK_SIZE))                                                                                                         
            cursor = dropbox.files.UploadSessionCursor(session_id=sess.session_id,                                                                                            
                                       offset=f.tell())                                                                                                                       
            commit = dropbox.files.CommitInfo(path=dropbox_path)                                                                                                              
                                                                                                                                                                              
            while f.tell() < file_size:                                                                                                                                       
                if ((file_size - f.tell()) <= CHUNK_SIZE):                                                                                                                    
                    print (dbx.files_upload_session_finish(f.read(CHUNK_SIZE), cursor, commit))                                                                               
                else:                                                                                                                                                         
                    dbx.files_upload_session_append(f.read(CHUNK_SIZE),                                                                                                       
                                    cursor.session_id,                                                                                                                        
                                    cursor.offset)                                                                                                                            
                    cursor.offset = f.tell()                                                                                                                                  
        print "Successfully uploaded ",sys.argv[1]
    except Exception as e:                                                                                                                                                    
        raise                 
