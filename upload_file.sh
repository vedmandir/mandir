ENV=/data/home/ubuntu/env
APP_DIR=/data/home/ubuntu/env/src/mandir
FILE_DATE=`date +%Y_%m_%d`
OUT_FILE=${APP_DIR}/dat/ved_${FILE_DATE}.json

cd $APP_DIR
${ENV}/bin/python manage.py dumpdata ved > ${OUT_FILE}
${ENV}/bin/python ${APP_DIR}/upload_file.py ${OUT_FILE}

