ENV=/home/dna/benv
APP_DIR=/home/dna/benv/mandir/
FILE_DATE=`date +%Y_%m_%d`
LOG_FILE=${APP_DIR}/log/event_email.${FILE_DATE}

cd $APP_DIR
date
echo "Running DRAFT"
echo "-------------"
${ENV}/bin/python manage.py send_event_email DRAFT 0 >> ${LOG_FILE}_DRAFT.log 2>&1
date
echo "-------------"
echo "Draft Finished"

echo "-------------"
echo "Running Real"
echo "-------------"
${ENV}/bin/python manage.py send_event_email REAL 0 >> ${LOG_FILE}_REAL.log 2>&1
date
echo "-------------"
echo "Real finished"
echo "-------------"
