from django.shortcuts import render
from models import Event, YearlyEvent, Darshan, Photos, DailyEvent, Donations, Newsletter, ExecutiveCouncils, LiveEventKey
from datetime import datetime,date,timedelta
from dateutil.relativedelta import relativedelta
from django.db.models import Q
from django.shortcuts import render
from forms import ContactForm, HallForm, PriestServiceForm, NewsletterForm
from django.core.mail import send_mail, EmailMessage
from django.conf import settings
from django.http import HttpResponse
import dropbox
import magic
from django.db.models import Q
import requests                                                                                                            
import json                                                                                                                
import httplib2

# Create your views here.

def index(request):
    context = {}
    return render(request, 'ved/index.html', context)

def datedisp(request,pmonth,pday):
    context = {}
    temp = "ved/dates/" + str(pmonth).zfill(2) + str(pday).zfill(2) + ".html"
    return render(request, temp, context)

def events(request, pmonth = None, pyear = None):
    today_date = datetime.now()
    from_date = date(today_date.year,today_date.month,1)
    to_date = from_date + relativedelta(months=6)
    #from_date = from_date + relativedelta(months=-1)
    from_date = from_date 
    
    today_date = today_date + relativedelta(days=-1)
    events = Event.objects.filter((Q(event_date__gte = from_date) & Q(event_date__lt = to_date)) | (Q(event_date__lte = today_date) & Q(event_end_date__gte = today_date))).order_by('event_date')
    return render(request, 'ved/events.html', {'events':events})

def eventsthisyear(request, pmonth = None, pyear = None):
    today_date = datetime.now()
    from_date = date(today_date.year,1,1)
    to_date = from_date + relativedelta(months=12)
    #from_date = from_date + relativedelta(months=-1)
    from_date = from_date 
    
    today_date = today_date + relativedelta(days=-1)
    events = Event.objects.filter((Q(event_date__gte = from_date) & Q(event_date__lt = to_date)) | (Q(event_date__lte = today_date) & Q(event_end_date__gte = today_date))).order_by('event_date')
    return render(request, 'ved/events.html', {'events':events})

def eventssincelastyear(request, pmonth = None, pyear = None):
    today_date = datetime.now()
    from_date = date(today_date.year,1,1)
    from_date = from_date + relativedelta(months=-12)
    to_date = from_date + relativedelta(months=24)
    
    events = Event.objects.filter((Q(event_date__gte = from_date) & Q(event_date__lt = to_date)) ).order_by('event_date')
    return render(request, 'ved/events.html', {'events':events})

def yearlyevents(request, pmonth = None):
    curr_year = datetime.now().year 
    today_date = datetime.now()
    from_date = date(today_date.year,1,1)
    if pmonth:
        events = YearlyEvent.objects.filter(event_date__gte = from_date,event_date__month = pmonth).order_by('event_date')
    else:
        events = YearlyEvent.objects.filter(event_date__gte = from_date).order_by('event_date')
        
    return render(request, 'ved/yearly_events.html', {'yearlyevents':events})

def lastyearevents(request, pmonth = None):
    curr_year = datetime.now().year  - 2
    if pmonth:
        events = YearlyEvent.objects.filter(event_date__year = curr_year,event_date__month = pmonth).order_by('event_date')
    else:
        events = YearlyEvent.objects.filter(event_date__year = curr_year).order_by('event_date')
        
    return render(request, 'ved/yearly_events.html', {'yearlyevents':events})

def dailyevents(request):
    events = DailyEvent.objects.all().order_by('event_order')
        
    return render(request, 'ved/daily_events.html', {'events':events})


def donations(request):
    donations = Donations.objects.all().order_by('pooja_type')
    return render(request, 'ved/donations.html', {'donations':donations})

def councils(request):
    councils = ExecutiveCouncils.objects.all().order_by('display_order','name')
    return render(request, 'ved/councils.html', {'councils':councils})

def darshan(request):
    darshans = Darshan.objects.filter(Q(display_order__gt = 100)).order_by('display_order')
    return render(request, 'ved/darshan.html', {'darshans':darshans})

def photos(request):
    return render(request, 'ved/photos.html', {})

def demo_light_skin(request):
    photos = Photos.objects.all().order_by('display_order')
    return render(request, 'ved/demo_light_skin.html', {'photos':photos})

def volunteer(request):
    return render(request, 'ved/vol.html', {})

def programs(request):
    return render(request, 'ved/programs.html', {})

def priest(request):
    if request.method == 'POST':
        form = PriestServiceForm(request.POST)

        if form.is_valid():
            form.save()

            priestrequest = form.instance

            mandir_staff = getattr(settings, "MANDIR_RESERVATION", "MANDIR_STAFF")

            ## Send Thank you email - not sending anymore to avoid spams
            #message = "Dear " + priestrequest.name + "\n"
            #message += "Thank you for your inquiry. Someone will get back to you as soon as possible."
            #send_mail("Thank you from Ved Mandir!", message, mandir_staff[0], [priestrequest.email])

            ## Notify Ved mandir staff
            message = "New Priest service request received from: " + priestrequest.name + "\n"
            message += "Email: " + priestrequest.email + "\n"
            message += "Phone: " + priestrequest.phone + "\n"
            message += "Pooja date: " + priestrequest.pooja_date + "\n"
            message += "Pooja time: " + priestrequest.pooja_time + "\n"
            message += "Pooja type: " + priestrequest.pooja_type + "\n"
            message += "Pooja location: " + priestrequest.pooja_location + "\n"
            message += "Comments: " + priestrequest.comments + "\n"
            message += "-----------------------------------------------------\n"
            #send_mail("New Priest Service Request", message, mandir_staff[0], mandir_staff)
            headers = {'Reply-To': priestrequest.email}
            msg  = EmailMessage("New Priest Service Request", message, mandir_staff[0], mandir_staff, headers=headers)
            msg.send()

            return render(request, 'ved/priest.html', {} )
        else:
            return render(request, 'ved/priest.html', {'form':form} )
            
    else:
        form = PriestServiceForm()
        return render(request, 'ved/priest.html', {'form':form} )

def hall(request):
    if request.method == 'POST':
        form = HallForm(request.POST)

        if form.is_valid():
            form.save()

            hallbooking = form.instance

            mandir_staff = getattr(settings, "MANDIR_RESERVATION", "MANDIR_STAFF")

            ## Send Thank you email - not sending anymore to avoid spams
            #message = "Dear " + hallbooking.name + "\n"
            #message += "Thank you for your inquiry. Someone will get back to you as soon as possible."
            #send_mail("Thank you from Ved Mandir!", message, mandir_staff[0], [hallbooking.email])

            ## Notify Ved mandir staff
            message = "New Hall booking request received from: " + hallbooking.name + "\n"
            message += "Email: " + hallbooking.email + "\n"
            message += "Phone: " + hallbooking.phone + "\n"
            message += "Booking date: " + hallbooking.booking_date + "\n"
            message += "Booking time: " + hallbooking.booking_time + "\n"
            message += "Priest service requested: " + ("YES" if hallbooking.priest_service_requested else "NO") + "\n"
            message += "Comments: " + hallbooking.comments + "\n"
            message += "-----------------------------------------------------\n"

            #send_mail("New Hall Booking Request", message, mandir_staff[0], mandir_staff)
            headers = {'Reply-To': hallbooking.email}
            msg  = EmailMessage("New Hall Booking Request", message, mandir_staff[0], mandir_staff, headers=headers)
            msg.send()

            return render(request, 'ved/hall.html', {} )
        else:
            return render(request, 'ved/hall.html', {'form':form} )
            
    else:
        form = HallForm()
        return render(request, 'ved/hall.html', {'form':form} )

def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)

        if form.is_valid():
            form.save()
            contact = form.instance

            mandir_staff = getattr(settings, "MANDIR_INFO", "MANDIR_STAFF")

            ## Send Thank you email - not sending anymore to avoid spam
            #message = "Dear " + contact.name + "\n"
            #message += "Thank you for your inquiry. Someone will get back to you as soon as possible."
            #send_mail("Thank you from Ved Mandir!", message, mandir_staff[0], [contact.email])

            ## Notify Ved mandir staff
            message = "New Contact message received from: " + contact.name + "\n"
            message += "Email: " + contact.email + "\n"
            message += "Subject: " + contact.subject + "\n"
            message += "Message: \n"
            message += contact.message  + "\n"
            message += "-----------------------------------------------------\n"
            #send_mail("New Contact Message", message, mandir_staff[0], mandir_staff,headers=headers)

            headers = {'Reply-To': contact.email}
            msg  = EmailMessage(contact.subject, message, mandir_staff[0], mandir_staff, headers=headers)
            #msg.content_subtype = "html"
            msg.send()

            return render(request, 'ved/contact.html', {} )
        else:
            return render(request, 'ved/contact.html', {'form':form} )
            
    else:
        form = ContactForm()
        return render(request, 'ved/contact.html', {'form':form} )

def newsletter(request):
    if request.method == 'POST':
        form = NewsletterForm(request.POST)

        if form.is_valid():
            newsletter = form.instance

            try:
                n = Newsletter.objects.get(email = newsletter.email)
                n.first_name = newsletter.first_name
                n.last_name = newsletter.last_name
                n.phone = newsletter.phone
                n.bounced = False
                n.unsubscribed = False
                n.save()
            except:
                form.save()

            mandir_staff = getattr(settings, "MANDIR_STAFF", None)

            ## Send Thank you email - not sending thank you - to avoid spam
            #message = "Dear " + newsletter.first_name + ",\n\n" + "You are signedup for Ved Mandir Newsletters! If you do not receive emails from Ved Mandir about upcoming events, please check your spam/junk folder and mark noreply@vedmandir.org as a safe sender.\n\nThank you.\nVed Mandir\n"
            #send_mail("Thank you from Ved Mandir!", message, 'Ved Mandir<' + mandir_staff[0] + '>', [newsletter.email])

            ## Notify Ved mandir staff
            message = "New Newsletter signup " + "\n"
            message += "Subject: Newsletter signup" + "\n"
            message += "From: " + newsletter.first_name + " " + newsletter.last_name + "\n"
            message += "Email: " + newsletter.email  + "\n"
            message += "-----------------------------------------------------\n"
            send_mail("New Newsletter signup", message, 'Ved Mandir<' + mandir_staff[0] + '>', mandir_staff)

            ## try adding to vertical response
            try:
                vr_auth_code = getattr(settings,"VR_AUTH_CODE",None)
                vr_list_id = getattr(settings,"VR_VED_LIST_ID", None)

                if vr_auth_code != None and vr_list_id != None:
                    headers = {'Authorization': vr_auth_code }
                    data = {'email': newsletter.email, 'first_name':newsletter.first_name,'last_name':newsletter.last_name} 
                    resp = requests.post('http://vrapi.verticalresponse.com/api/v1/contacts', headers=headers, data=data)                      

                    data = {'email': newsletter.email}
                    resp = requests.post('https://vrapi.verticalresponse.com/api/v1/lists/' + vr_list_id + '/contacts', headers=headers, data=data)
                else:
                    print "VR ATTRS not found"

            except Exception as e:
                print e
                pass

            return render(request, 'ved/newsletter.html', {} )
        else:
            return render(request, 'ved/newsletter.html', {'form':form} )
            
    else:
        form = NewsletterForm()
        return render(request, 'ved/newsletter.html', {'form':form} )

def avail(request):
    return render(request, 'ved/avail.html', {})

def showpdf(request,filename):
    filename = 'flyers/pdf/' + filename + ".pdf"
    with open(filename, 'r') as pdf:
        response = HttpResponse(pdf.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'inline;filename=some_file.pdf'
        return response
    pdf.closed

def showflyer(request,filename):
    try:
        drop_path = "/" + filename
        auth_key = getattr(settings, "DROPBOX_KEY", None)
        dbx = dropbox.Dropbox(auth_key)
        md, res = dbx.files_download(drop_path)
        mime = magic.Magic(mime=True)
        ct = mime.from_buffer(res.content)
        response = HttpResponse(res.content, content_type=ct)
        response['Content-Disposition'] = 'inline;filename=' + filename
        return response
    except:
        raise

def about(request):
    return render(request, 'ved/about.html', {})

def socialposts(request):
    try:
        fd = datetime(datetime.now().year, datetime.now().month, datetime.now().day)
        td = td = fd + relativedelta(days=1)

        keyobj = LiveEventKey.objects.filter(eventDate__gte = fd).filter(eventDate__lt = td).latest()
        keyid = keyobj.keyId
    except:
        keyid = '<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FVedMandirNJ%2F&tabs=timeline&width=500&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true" width="500" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>'

    print keyid
    return render(request, 'ved/socialposts.html', {'keyid':keyid})


