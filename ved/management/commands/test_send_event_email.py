# coding: utf-8
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import smtplib,os
from django.core.management.base import BaseCommand, CommandError
from ved.models import Event, Newsletter, DraftEmailList
from datetime import datetime,date,timedelta
from dateutil.relativedelta import relativedelta
from django.core.mail import send_mail
from django.conf import settings
import dropbox
import magic
from django.db.models import Q
import requests
from subprocess import call
import sys, time

class Command(BaseCommand):
    def add_arguments(self,parser):
        parser.add_argument('email_type')
        parser.add_argument('event_id')

    def handle(self, *args, **options):

        email_type = options["email_type"]
        event_id = int(options["event_id"])

        no_days = getattr(settings, "DAYS_BEFORE_EVENT_EMAIL", 3)

        #email_list = ['bounce@simulator.amazonses.com']
        email_list = ['dharmesh.adhia@gmail.com']

        print "List..."
        print email_list

        from_date = datetime.today().date()

        if email_type == "DRAFT":
            to_date = from_date + relativedelta(days=no_days)
        else:
            to_date = from_date


        print "Getting events for ", to_date

        if event_id == 0:
            #events = Event.objects.filter(event_date = to_date).filter(Q(description1_link = True) | Q(description2_link = True))
            events = Event.objects.filter(Q(email_date_1 = to_date) | Q(email_date_2 = to_date) | Q(email_date_3 = to_date) | Q(email_date_4 = to_date)).filter(Q(description1_link = True) | Q(description2_link = True))
        else:
            events = Event.objects.filter(id = event_id).filter(Q(description1_link = True) | Q(description2_link = True))
            
        if len(events) == 0:
            print "No events scheduled"
            sys.exit(0)

        auth_key = getattr(settings, "DROPBOX_KEY", None)
        dbx = dropbox.Dropbox(auth_key)

        for event in events:
            filename = event.description1 if event.description1_link else event.description2
            drop_path = "/" + filename
            md, res = dbx.files_download(drop_path)
            nf = open ('/tmp/' + filename, "wb")
            nf.write (res.content)
            nf.close()

            jpg_file = filename.replace(".pdf","")
            jpg_file = jpg_file + ".jpg"
            call(["convert", "-density", "100", "/tmp/" + filename ,"/tmp/" + jpg_file])

            self.send_event(event, email_type, email_list, "/tmp/" + jpg_file, filename)


    def send_event(self, event, email_type, email_list, file_path, pdf_filename):

        ## No attachments at this time
        files = []

        send_from = getattr(settings, "MANDIR_STAFF", [])[0]

        if email_type == "DRAFT":
            subject = "TEST -- Ved Mandir - " + event.description + " - " + event.event_date.strftime("%B %d, %Y")
        else:
            subject = "TEST -- Ved Mandir - " + event.description + " - " + event.event_date.strftime("%B %d, %Y")


        print "Send from: ", send_from
        msg = MIMEMultipart('related')
        msg['From'] = "Ved Mandir <" + send_from + ">"
        #msg['To'] = [send_from]
        #msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = subject

        msg.add_header('reply-to', send_from)
        msg.add_header('X-SES-MESSAGE-TAGS', 'campaign=' + event.event_date.strftime("%d-%B-%Y"))
        msg.add_header('X-SES-CONFIGURATION-SET', 'email-delivery')

        msgAlternative = MIMEMultipart('alternative')
        msg.attach(msgAlternative)

        if event.program_text != None:
            prgText = MIMEText(event.program_text.encode('ascii',errors='ignore'),'plain')
            msgAlternative.attach(prgText)


        #msgText = MIMEText('<b><br><img src="cid:image1"><a href="http://www.vedmandir.org/ved/flyer/VedMandirNotice.pdf">Click to join</a>', 'html')
        htmlstr = '<b><br><a href="http://www.vedmandir.org/ved/flyer/' + pdf_filename + '"><img src="cid:image1"></a>'
        msgText = MIMEText(htmlstr, 'html')
        #msgText = MIMEText('<b><table width="100%" cellspacing="0" cellpadding="0"> <tr> <td style="text-align: center; vertical-align: middle;"> <table cellspacing="0" cellpadding="0"> <tr> <td style="border-radius: 2px;" bgcolor="#ED2939"> <a href="https://globalgatewaye4.firstdata.com/collect_payment_data?ant=c3ba94457725e34330f1902675309353&merchant=WSP-VED-M-aHWNMwB71A&order=50e86c72edc7c52c3527e0011311743844526192e1f2d2091f2ca66a71276c86&t=1" target="_blank" style="padding: 8px 12px; border: 1px solid #ED2939;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;"> Donate </a> </td> </tr> </table> </td> </tr> <tr><a href="http://www.vedmandir.org/ved/flyer/' + pdf_filename + '><img src="cid:image1"></a>', 'html')
        msgAlternative.attach(msgText)

        text = MIMEImage(open(file_path,"rb").read())
        text.add_header('Content-ID', '<image1>')
        text.add_header('Content-Disposition', 'attachment',filename = os.path.basename(file_path))
        msg.attach(text)

        for f in files:
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(f, "rb").read())
            Encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
            msg.attach(part)

        email_server = getattr(settings, "EMAIL_HOST", 'smtp.gmail.com')
        email_port = getattr(settings, "EMAIL_PORT", 587)
        email_host_user = getattr(settings, "EMAIL_HOST_USER", None)
        email_host_password = getattr(settings, "EMAIL_HOST_PASSWORD", None)

        i = 0
        max_batch_size = 45
        total_count = len(email_list)

        send_to = [send_from]

        while i < total_count:
            sub_list = email_list[i:i+max_batch_size]
            print "sub_list"
            print sub_list
            i = i + max_batch_size

            for attempt in range(3):
                try:
                    server = smtplib.SMTP(email_server, email_port)
                    server.ehlo()
                    server.starttls()
                    server.login(email_host_user, email_host_password)

                    server.sendmail(send_from, send_to + sub_list, msg.as_string())
                    server.close()
                    print "Attempt: ", attempt + 1, " successful"
                    time.sleep(5)
                    break
                except Exception as e:
                    print "Attempt: ", attempt + 1, " failed"
                    print str(e)
                    time.sleep(10*attempt)

