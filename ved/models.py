from django.db import models

# Create your models here.

class Event(models.Model):
    description = models.CharField(max_length=200)
    description1 = models.CharField(max_length=200,  blank = True)
    description1_link = models.BooleanField(default = False)
    description2 = models.CharField(max_length=200,  blank = True)
    description2_link = models.BooleanField(default = False)
    event_date = models.DateField('event start')
    event_end_date = models.DateField('event end',blank = True, null = True)
    program_text = models.TextField(null = True, blank = True)
    email_date_1 = models.DateField('email send date 1',blank = True, null = True)
    email_date_2 = models.DateField('email send date 2',blank = True, null = True)
    email_date_3 = models.DateField('email send date 3',blank = True, null = True)
    email_date_4 = models.DateField('email send date 4',blank = True, null = True)

class YearlyEvent(models.Model):
    description = models.CharField(max_length=200)
    description1 = models.CharField(max_length=200, null = True, blank = True)
    description2 = models.CharField(max_length=200, null = True, blank = True)
    event_date = models.DateField('event start')

class DailyEvent(models.Model):
    event_day =  models.CharField(max_length=16)
    time1 = models.CharField(max_length=64)
    time2 = models.CharField(max_length=64, null = True, blank = True)
    time3 = models.CharField(max_length=64, null = True, blank = True)
    description1 = models.CharField(max_length=200)
    description2 = models.CharField(max_length=200, null = True, blank = True)
    description3 = models.CharField(max_length=200, null = True, blank = True)
    footnote = models.CharField(max_length=200, null = True, blank = True)
    event_order = models.IntegerField()

class Darshan(models.Model):
    image_name = models.CharField(max_length=200)
    label = models.CharField(max_length=200, null = True, blank = True)
    data_type = models.CharField(max_length=50, null = True, blank = True)
    display_order = models.IntegerField(null = True)

class Photos(models.Model):
    image_name = models.CharField(max_length=200)
    small_image_name = models.CharField(max_length=200, null=True)
    display_order = models.IntegerField(null = True)
    external_link = models.BooleanField(default = False)

class Contact(models.Model):
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length = 200, verbose_name = "Name")
    email = models.EmailField()
    subject  = models.CharField(max_length = 100)
    message = models.TextField(null = True, blank = True)

class HallBooking(models.Model):
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length = 200)
    email = models.EmailField()
    phone  = models.CharField(max_length = 30)
    booking_date = models.CharField(max_length = 200,verbose_name = "Reservation Date")
    booking_time = models.CharField(max_length = 200,verbose_name = "Reservation Time")
    priest_service_requested = models.BooleanField(verbose_name = 'Priest Service Required?')
    comments = models.TextField(null = True, blank = True)
    
class PriestService(models.Model):
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length = 200)
    email = models.EmailField()
    phone  = models.CharField(max_length = 30)
    pooja_date = models.CharField(max_length=100,verbose_name = "Pooja Date")
    pooja_time = models.CharField(max_length=100,verbose_name = "Pooja Time")
    pooja_type = models.CharField(max_length=100,verbose_name = "Pooja Type")
    pooja_location = models.CharField(max_length=100,verbose_name = "Location of Pooja")
    comments = models.TextField(null = True, blank = True)

class Donations(models.Model):
    pooja_type = models.CharField(max_length=200)
    in_mandir_price = models.DecimalField(max_digits=6, decimal_places=2,blank=True,null=True)
    outside_mandir_price = models.DecimalField(max_digits=6, decimal_places=2,blank=True,null=True)
    display_order= models.IntegerField()
    bold = models.BooleanField(default = False)

       
class Newsletter(models.Model):
    added = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    email = models.EmailField()
    first_name = models.CharField(max_length = 200, null = True, blank = True)
    last_name = models.CharField(max_length = 200, null = True, blank = True)
    phone  = models.CharField(max_length = 30, null = True, blank = True)
    bounced = models.BooleanField(default = False)
    unsubscribed = models.BooleanField(default = False)

class DraftEmailList(models.Model):
    email = models.EmailField()
    first_name = models.CharField(max_length = 200, null = True, blank = True)
    last_name = models.CharField(max_length = 200, null = True, blank = True)

class ExecutiveCouncils(models.Model):
    name = models.CharField(max_length = 200)
    display_order= models.IntegerField(null=True,blank=True)

class LiveEventKey(models.Model):
    class Meta:
        get_latest_by = 'id'
    keyId = models.CharField(max_length = 1024, null = True, blank = True)
    eventDate = models.DateTimeField(null = True)



