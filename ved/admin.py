from django.contrib import admin

# Register your models here.
from models import Event
from django.contrib import admin

from .models import Event, YearlyEvent, Darshan, Photos, Contact, HallBooking, PriestService, DailyEvent, Donations, ExecutiveCouncils, Newsletter, DraftEmailList, LiveEventKey

class EventAdmin(admin.ModelAdmin):
        list_display = ('event_date','description','email_date_1','email_date_2')

class YearlyEventAdmin(admin.ModelAdmin):
        list_display = ('event_date','description')

class DailyEventAdmin(admin.ModelAdmin):
        list_display = ('event_day','description2')

class DarshanAdmin(admin.ModelAdmin):
        list_display = ('image_name','label','data_type','display_order')

class PhotosAdmin(admin.ModelAdmin):
        list_display = ('image_name','small_image_name','display_order')

class ContactAdmin(admin.ModelAdmin):
        list_display = ('name','email','subject','message')

class HallBookingAdmin(admin.ModelAdmin):
        list_display = ('name','email','phone','booking_date','booking_time','priest_service_requested')

class PriestServiceAdmin(admin.ModelAdmin):
        list_display = ('name','email','phone','pooja_date','pooja_time','pooja_type','pooja_location')

class DonationsAdmin(admin.ModelAdmin):
        list_display = ('pooja_type','in_mandir_price','outside_mandir_price','display_order')

class ExecutiveCouncilsAdmin(admin.ModelAdmin):
        list_display = ('name','display_order')

class NewsletterAdmin(admin.ModelAdmin):
        list_display = ('email','added','bounced','unsubscribed')
        search_fields = ('first_name','last_name','email')

class DraftEmailListAdmin(admin.ModelAdmin):
        list_display = ('email','first_name','last_name')

class LiveEventKeyAdmin(admin.ModelAdmin):
        list_display = ('keyId','eventDate')

admin.site.register(Event,EventAdmin)
admin.site.register(DailyEvent,DailyEventAdmin)
admin.site.register(YearlyEvent,YearlyEventAdmin)
admin.site.register(Darshan,DarshanAdmin)
admin.site.register(Photos,PhotosAdmin)
admin.site.register(Contact,ContactAdmin)
admin.site.register(HallBooking,HallBookingAdmin)
admin.site.register(ExecutiveCouncils,ExecutiveCouncilsAdmin)
admin.site.register(PriestService,PriestServiceAdmin)
admin.site.register(Donations,DonationsAdmin)
admin.site.register(Newsletter,NewsletterAdmin)
admin.site.register(DraftEmailList,DraftEmailListAdmin)
admin.site.register(LiveEventKey,LiveEventKeyAdmin)

